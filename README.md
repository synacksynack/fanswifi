* Bootstrap ansible

Ansible has two main distributions modes: github, and pip.
I wouldn't recommend to use github, on your case. The latest stable release can
be installed from pip, plus it would pull all dependencies by itself, you won't
have to worry about how ansible actually works, ...

Installation instructions can be found here
http://docs.ansible.com/ansible/intro_installation.html#latest-releases-via-pip

They assume you either have pip or easy_install available on your system.
I'm no mac user, I couldn't tell. If you need help installing either of these,
let me know.

Anyway, from there, we'll assume you have ansible commands running


* Add a new host

Add hosts to the ./hosts file (either an IP, or a DNS)

On a new host: YOU NEED to have python-apt installed
It's not usually there on debian. It is on default ubuntu installs. If package
installation tasks don't work, start by checking for this package status.

To use interactive password prompt, you need sshpass to be installed on your
system. I'm not sure how to do this on mac, and passwords are not recommended,
so we'll go straight to using SSH keys, allowing you to bypass password
authentication.
If you don't have some SSH key already, you may generate one from some terminal
emulator/console, running the command ssh-keygen -t rsa
You'll be prompted for a few questions, you may hit enter a few times, until
ssh-keygen finishes creating your key.
You now have a couple files, ~/.ssh/id_rsa and ~/.ssh/id_rsa.pub, your private
and public keys respectively. The content of the latter file has to be installed
on your server's /root/authorized_keys, so your local user may SSH to remote
servers without having to provide remote root's password. Maybe you're already
familiar with that, it's pretty common, ... if you're not, that's pretty much it.

Anyway, you may send your public keys using an other command: ssh-copy-id -i ~/.ssh/id_rsa.pub root@my.new.server.ip
You may be asked to allow communications with remote host 'identified by some
key' (ssh hosts keys, nothing to do with your ssh keys). Then, to enter remote's
root password. Upon successful authentication, your ~/.ssh/id_rsa.pub is
installed on remote root account authorized_keys file, so you won't have to
enter your password again.

You may then check passwordless ssh login works: ssh root@my.new.server.ip
You should now be able to access your server's shell with no further action.
Our test worked, you may close this session.


* Run ansible to bootstrap your servers

You have ansible installed on your system.
You have SSH keys allowing you to connect to your boxes without having to enter
your password.
Your remote systems have python-apt installed (with Ubuntu: yes)

Open a terminal emulator/console and go to your ansible playbook root directory:
cd ~/fanswifi

The ./hosts file may or may not already have your remote hosts defined:
cat hosts
[fanswifi-unbound]
#127.0.0.1
#161.202.32.212
161.202.165.174

You can comment a target with a #. The file format is INI, nothing out of the
ordinary.

To check connectivity, before applying your configuration, you may use the
following:
ansible -vvv -i hosts -m ping -u root fanswifi-unbound

You could remove the -vvv (very verbose, could help identifying your problem).
In the end, you should see your targets returning 'pong'. If so, you're ready.

To apply configuration on a remote host, you'll need to use:
ansible -vvv -i hosts -m ping -u root fanswifi-unbound

I've just applied this playbook on your new server. not yet on your old one (a
few configuration changes, mostly removing commentaries from configuration
files, a few commented directives I removed when adding fail2ban, ...)
You could find the complete output running the former command, in proof.txt
